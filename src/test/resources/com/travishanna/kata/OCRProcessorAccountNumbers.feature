Feature: Read Account Numbers
  As an OCRProcessor user
  I want the program to read ascii art account numbers
  so that I don't have to type them in

Scenario: Process Sample File
  When I process the sample file named "test-input.txt"
  Then I should get output which matches the contents of the file "expected-output.txt"