Feature: Display Usage
  As an OCRProcessor user
  I want the program to display usage information if I call it with invalid arguments
  so that I can learn how the program works

Scenario: No arguments
  When I launch OCRProcessor with no arguments
  Then program usage should be displayed

Scenario: File not found
  When I launch OCRProcessor against a file that doesn't exist
  Then I should see a file not found error