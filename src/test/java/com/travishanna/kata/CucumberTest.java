package com.travishanna.kata;

import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;

/**
 * Kicks off all cucumber tests
 * Author: Travis Hanna
 */
@RunWith(Cucumber.class)
@Cucumber.Options(format = {"pretty", "html:target/cucumber-html-report", "json-pretty:target/cucumber-json-report.json"})
public class CucumberTest {
}
