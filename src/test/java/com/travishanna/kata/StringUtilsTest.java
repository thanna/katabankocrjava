package com.travishanna.kata;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Author: Travis Hanna
 */
public class StringUtilsTest {

    @Test
    public void testReplaceChars() {
        assertEquals("swap character", StringUtils.replaceChar("slap character", 1, 'w'));
    }

}
