package com.travishanna.kata;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CharacterUtilsTest {

	@Test
	public void testToggleBetweenCharacterAndSpace() {
		assertEquals("Toggl Something", CharacterUtils.toggleBetweenCharacterAndSpace("ToggleSomething", 5, 'e'));
		assertEquals("ToggleSomething", CharacterUtils.toggleBetweenCharacterAndSpace("Toggl Something", 5, 'e'));
	}
	
	@Test
	public void testCharacterArrayToCharacterSequence() {
		assertEquals("Sequence", CharacterUtils.characterArrayToCharacterSequence(
				new char[][]{
					{'S', 'e', 'q'},
					{'u', 'e', 'n'},
					{'c', 'e'}
				})
		);
	}

    @Test
    public void testStringArrayToCharArray() {
        char[][] expectedArray = new char[][]{
            {'a', 'b', 'c'},
            {'d', 'e', 'f'},
            {'g', 'h', 'i'}
        };
        assertArrayEquals(expectedArray, CharacterUtils.stringArrayToCharArray(new String[]{"abc", "def", "ghi"}));
    }

    @Test
    public void testSplitMultiDimensionalArrayVertically() {
        List<char[][]> expectedResult = new ArrayList<char[][]>();
        expectedResult.add(new char[][]{{'1', '1'}, {'1', '1'}});
        expectedResult.add(new char[][]{{'2', '2'}, {'2', '2'}});

        List<char[][]> resultToTest = CharacterUtils.splitMultiDimensionalArrayVertically(
            new char[][] {
                {'1', '1', '2', '2'},
                {'1', '1', '2', '2'}
            }, 2
        );

        assertTrue(resultToTest.size() == 2);
        for (int i = 0; i < resultToTest.size(); i++)
        {
            assertArrayEquals(expectedResult.get(i), resultToTest.get(i));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSplitArrayIrregularWidth() {
        CharacterUtils.splitMultiDimensionalArrayVertically(new char[][]{{'a','a'},{'b'}},2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSplitArrayNotDivisibleByWidth() {
        CharacterUtils.splitMultiDimensionalArrayVertically(new char[][]{{'a','a','a'},{'b','b','b'}},2);
    }

}
