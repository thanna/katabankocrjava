package com.travishanna.kata;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Author: Travis Hanna
 */
public class AccountNumberTest {
    @Test
    public void testIsValid() {
        AccountNumber validAccount = new AccountNumber("457508000");
        assertTrue(validAccount.isValid());

        AccountNumber invalidAccount = new AccountNumber("664371495");
        assertFalse(invalidAccount.isValid());
    }

    @Test
    public void testToString() {
        AccountNumber validAccount = new AccountNumber("457508000");
        assertEquals("457508000", validAccount.toString());
    }
}
