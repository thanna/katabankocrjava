package com.travishanna.kata;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Author: Travis Hanna
 */
public class AsciiArtCharacterTest {
    private final Map<Character,char[][]> testData = new HashMap<Character, char[][]>();
    private final char[][] unrecognized = new char[][] {
        {' ', ' ', ' '},
        {' ', ' ', ' '},
        {' ', ' ', ' '}
    };
    private final char[][] invalidCharacter = new char[2][3];

    @Test
    public void testParseCharArray() {
        for (Character c : testData.keySet()) {
            AsciiArtCharacter parsedCharacter = AsciiArtCharacter.parseCharArray(testData.get(c));
            assertEquals(c.charValue(), parsedCharacter.getCharacter());
            assertTrue(parsedCharacter.isRecognized());
        }
    }

    @Test
    public void testParseUnrecognizedCharacter() {
        AsciiArtCharacter parsedCharacter = AsciiArtCharacter.parseCharArray(unrecognized);
        assertEquals('?', parsedCharacter.getCharacter());
        assertFalse(parsedCharacter.isRecognized());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseCharArrayInvalidInput() {
        AsciiArtCharacter.parseCharArray(invalidCharacter);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseCharArrayNullInput() {
        AsciiArtCharacter.parseCharArray(null);
    }

    @Test
    public void testPossibleSubstitutions() {
        AsciiArtCharacter eight = AsciiArtCharacter.parseCharArray(testData.get('8'));
        Set<Character> possibleSubstitutions = eight.possibleSubstitutions();
        assertThat(possibleSubstitutions, hasItems('0', '6', '9'));
    }

    @Before
    public void setupTestData()
    {
        testData.put('0', new char[][]{
                {' ', '_', ' '},
                {'|', ' ', '|'},
                {'|', '_', '|'}
        });
        testData.put('1', new char[][]{
                {' ', ' ', ' '},
                {' ', ' ', '|'},
                {' ', ' ', '|'}
        });
        testData.put('2', new char[][]{
                {' ', '_', ' '},
                {' ', '_', '|'},
                {'|', '_', ' '}
        });
        testData.put('3', new char[][]{
                {' ', '_', ' '},
                {' ', '_', '|'},
                {' ', '_', '|'}
        });
        testData.put('4', new char[][]{
                {' ', ' ', ' '},
                {'|', '_', '|'},
                {' ', ' ', '|'}
        });
        testData.put('5', new char[][]{
                {' ', '_', ' '},
                {'|', '_', ' '},
                {' ', '_', '|'}
        });
        testData.put('6', new char[][]{
                {' ', '_', ' '},
                {'|', '_', ' '},
                {'|', '_', '|'}
        });
        testData.put('7', new char[][]{
                {' ', '_', ' '},
                {' ', ' ', '|'},
                {' ', ' ', '|'}
        });
        testData.put('8', new char[][]{
                {' ', '_', ' '},
                {'|', '_', '|'},
                {'|', '_', '|'}
        });
        testData.put('9', new char[][]{
                {' ', '_', ' '},
                {'|', '_', '|'},
                {' ', '_', '|'}
        });

    }
}
