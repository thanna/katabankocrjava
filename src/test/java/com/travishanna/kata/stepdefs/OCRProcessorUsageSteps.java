package com.travishanna.kata.stepdefs;

import com.travishanna.kata.testutils.OCRProcessorExecutor;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.*;

/**
 * Author: Travis Hanna
 */
public class OCRProcessorUsageSteps {
    private final OCRProcessorExecutor executor = new OCRProcessorExecutor();

    @When("^I launch OCRProcessor with no arguments$")
    public void I_launch_OCRProcessor_with_no_arguments() throws Throwable {
        executor.execute(new String[]{});
    }

    @Then("^program usage should be displayed$")
    public void program_usage_should_be_displayed() throws Throwable {
        assertTrue(this.executor.getConsoleOutput().contains("Usage:"));
    }

    @When("^I launch OCRProcessor against a file that doesn't exist$")
    public void I_launch_OCRProcessor_against_a_file_that_doesn_t_exist() throws Throwable {
        executor.execute((new String[]{"this_file_is_missing.txt"}));
    }

    @Then("^I should see a file not found error$")
    public void I_should_see_a_file_not_found_error() throws Throwable {
        assertNotEquals(0, executor.getStatusCode());
        assertTrue(executor.getErrOutput().contains("this_file_is_missing.txt not found"));
    }
}
