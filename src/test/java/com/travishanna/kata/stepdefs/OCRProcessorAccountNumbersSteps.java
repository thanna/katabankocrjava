package com.travishanna.kata.stepdefs;

import com.travishanna.kata.testutils.FileUtils;
import com.travishanna.kata.testutils.OCRProcessorExecutor;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.*;

/**
 * Author: Travis Hanna
 */
public class OCRProcessorAccountNumbersSteps {
    private final OCRProcessorExecutor executor = new OCRProcessorExecutor();

    @When("^I process the sample file named \"([^\"]*)\"$")
    public void I_process_the_sample_file_named(String filename) throws Throwable {
        executor.execute(new String[]{"target/test-classes/" + filename});
        assertEquals(0, executor.getStatusCode());
    }

    @Then("^I should get output which matches the contents of the file \"([^\"]*)\"$")
    public void I_should_get_output_which_matches_the_contents_of_the_file(String filename) throws Throwable {
        String expectedResults = FileUtils.readFile("target/test-classes/" + filename);
        assertEquals(expectedResults, executor.getConsoleOutput());
    }
}
