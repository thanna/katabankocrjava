package com.travishanna.kata;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Author: Travis Hanna
 */
public class AsciiArtStringTest {

    @Test
    public void testParseStringArray()
    {
        String[] oneThroughNine = {" _     _  _     _  _  _  _  _ ",
                                   "| |  | _| _||_||_ |_   ||_||_|",
                                   "|_|  ||_  _|  | _||_|  ||_| _|"};
        AsciiArtString parsedString = AsciiArtString.parseStringArray(oneThroughNine);
        assertEquals("0123456789", parsedString.toString());
        assertEquals("0123456789", parsedString.getString());
        assertFalse(parsedString.hasUnrecognizedCharacter());
    }

    @Test
    public void testParseStringArrayWithUnrecognizedCharacter()
    {
        String[] oneThroughNine = {" _     _  _     _  _  _  _  _ ",
                                   "| |  | _|  ||_||_ |_   ||_||_|",
                                   "|_|  ||_  _|  | _||_|  ||_| _|"};
        AsciiArtString parsedString = AsciiArtString.parseStringArray(oneThroughNine);
        assertEquals("012?456789 ILL", parsedString.toString());
        assertEquals("012?456789", parsedString.getString());
        assertTrue(parsedString.hasUnrecognizedCharacter());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidStringArray() {
        AsciiArtString.parseStringArray(new String[2]);
    }

}
