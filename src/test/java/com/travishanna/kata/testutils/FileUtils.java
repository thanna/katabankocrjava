package com.travishanna.kata.testutils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * Author: Travis Hanna
 */
public class FileUtils {
    public static String readFile(String filename) {
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(new File(filename));
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            return Charset.defaultCharset().decode(bb).toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                stream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
