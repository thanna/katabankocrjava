package com.travishanna.kata.testutils;

import com.travishanna.kata.OCRProcessor;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * This class executes OCRProcessor with the specified arguments and captures the console consoleOutput
 * Author: thanna
 */
public class OCRProcessorExecutor {
    private String consoleOutput;
    private String errOutput;
    private int statusCode;

    /**
     * Executes OCRProcessor with the specified command line arguments.  This method temporarily
     * steals System.out so that it can capture the output which would have been sent to the
     * console
     */
    public void execute(String[] args)
    {
        PrintStream backupOutStream = System.out;
        PrintStream backupErrStream = System.err;
        ByteArrayOutputStream myOutStream = new ByteArrayOutputStream();
        ByteArrayOutputStream myErrStream = new ByteArrayOutputStream();
        PrintStream myPrintOutStream = new PrintStream(myOutStream);
        PrintStream myPrintErrStream = new PrintStream(myErrStream);

        System.setOut(myPrintOutStream);
        System.setErr(myPrintErrStream);

        this.statusCode = OCRProcessor.run(args);

        System.out.flush();
        System.err.flush();;

        this.consoleOutput = myOutStream.toString();
        this.errOutput = myErrStream.toString();

        System.setOut(backupOutStream);
        System.setErr(backupErrStream);
    }

    public String getConsoleOutput() {
        return consoleOutput;
    }

    public String getErrOutput() {
        return errOutput;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
