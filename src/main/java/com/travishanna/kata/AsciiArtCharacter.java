package com.travishanna.kata;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * This class represents an ascii art character composed via a 3x3 grid
 * For example, the character 3 would be represented as:
 *     _
 *     _|
 *     _|
 * 
 * This class is immutable.  Instances are created via the parsing method(s).  Once created
 * the class can tell you what character was parsed (if it recognized one).  It can also tell 
 * you which other characters are only one underscore/pipe away from the supplied data.  This
 * can be useful in automatic error correction.
 * 
 * Author: Travis Hanna
 */
public class AsciiArtCharacter {
    private static final Map<CharSequence, Character> sequenceMap;
    private static final Map<CharSequence, Set<Character>> substitutionMap;

    private char[][] data;
    private boolean recognized;
    private char character;
    
    static {
        sequenceMap = new HashMap<CharSequence, Character>();
        initializeSequenceMap();

        substitutionMap = new HashMap<CharSequence, Set<Character>>();
        initializeSubstitutionMap();
    }

    private AsciiArtCharacter(char[][] data) {
        this.character = '?';
        this.data = data;
        this.recognized = false;
    }

    private AsciiArtCharacter(char character, char[][] data) {
        this.character = character;
        this.data = data;
        this.recognized = true;
    }

    public static AsciiArtCharacter parseCharArray(char[][] chars) {
        if (chars == null) {
            throw new IllegalArgumentException("Cannot convert null to character");
        }

        if (chars.length != 3) {
            throw new IllegalArgumentException("This class only handles 3x3 arrays");
        }

        for (char[] row : chars) {
            if (row.length != 3) throw new IllegalArgumentException("This class only handles 3x3 arrays");
        }

        CharSequence sequence = CharacterUtils.characterArrayToCharacterSequence(chars);

        if (sequenceMap.containsKey(sequence)) {
            return new AsciiArtCharacter(sequenceMap.get(sequence), chars);
        }
        else {
            return new AsciiArtCharacter(chars);
        }
    }
    
    public Set<Character> possibleSubstitutions () {
        Set<Character> possibleSubstitutions = substitutionMap.get(CharacterUtils.characterArrayToCharacterSequence(this.data));
        return possibleSubstitutions == null ? new HashSet<Character>() : possibleSubstitutions;
    }

    private static void initializeSequenceMap()
    {
    	Properties props = new Properties();
    	
    	try {
    		props.load(AsciiArtCharacter.class.getClassLoader().getResourceAsStream("characterSet.properties"));
    		for (Object obj : props.keySet()) {
    			if (obj instanceof String) {
    				String key = (String) obj;
    				if (key.startsWith("character."));
    				sequenceMap.put((String)props.get(key), key.charAt(10));
    			}
    		}
    	}
    	catch (IOException e) {
    		throw new RuntimeException("Unable to read character set from characterSet.properties", e);
    	}
    }

    private static void initializeSubstitutionMap() {
        for (CharSequence sequence : sequenceMap.keySet()) {
            for (int i = 0; i < sequence.length(); i++) {
                if (i == 1 || i == 4 || i == 7) {
                	CharSequence substitution = CharacterUtils.toggleBetweenCharacterAndSpace(sequence, i, '_');
                    addToSubstitutionMap(substitution, sequenceMap.get(sequence));
                }
                else if (i == 3 || i == 5 || i == 6 || i == 8) {
                	CharSequence substitution = CharacterUtils.toggleBetweenCharacterAndSpace(sequence, i, '|');
                    addToSubstitutionMap(substitution, sequenceMap.get(sequence));
                }
            }
        }
    }
    
    private static void addToSubstitutionMap(CharSequence sequence, char character) {
        Set<Character> charSet = substitutionMap.get(sequence);
        if (charSet == null) {
            charSet = new HashSet<Character>();
        }
        charSet.add(character);
        substitutionMap.put(sequence, charSet);
    }

    public char getCharacter() {
        return character;
    }

    public boolean isRecognized() {
        return recognized;
    }
}
