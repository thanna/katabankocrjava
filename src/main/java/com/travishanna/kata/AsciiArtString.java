package com.travishanna.kata;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * This class parses multi-line ascii art text made up of 3x3 characters into strings. Example:
 *     _  _     _  _  _  _  _
 *   | _| _||_||_ |_   ||_||_|
 *   ||_  _|  | _||_|  ||_| _|
 *
 * This class is immutable.  Instances are created via the parse method(s).  Once created, the
 * instance can tell you what string was parsed.  It can also tell you what other strings are only
 * one underscore or pipe removed.  This is useful for automatic error correction
 *
 * Author: Travis Hanna
 */
public class AsciiArtString {
    private String string;
    private AsciiArtCharacter[] characters;
    private boolean hasUnrecognizedCharacter;

    private AsciiArtString(String string) {
        this.string = string;
    }

    public static AsciiArtString parseStringArray(String[] strings) {
        if (!isValidAsciiArt(strings)) {
            throw new IllegalArgumentException("This is not a valid ascii art string");
        }

        AsciiArtString retVal = null;
        boolean hasUnrecognizedCharacter = false;
        AsciiArtCharacter[] characters = new AsciiArtCharacter[strings[0].length() / 3];;

        char[][] chars = CharacterUtils.stringArrayToCharArray(strings);
        List<char[][]> splitArray = CharacterUtils.splitMultiDimensionalArrayVertically(chars, 3);
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < splitArray.size(); i++) {
            char[][] individualCharacter = splitArray.get(i);
            AsciiArtCharacter parsedCharacter = AsciiArtCharacter.parseCharArray(individualCharacter);
            characters[i] = parsedCharacter;
            builder.append(parsedCharacter.getCharacter());
            if (!parsedCharacter.isRecognized()) {
                hasUnrecognizedCharacter = true;
            }
        }

        retVal = new AsciiArtString(builder.toString());
        retVal.hasUnrecognizedCharacter = hasUnrecognizedCharacter;
        retVal.characters = characters;
        return retVal;
    }
    
    public Set<String> possibleSubstitutions() {
        Set<String> retVal = new LinkedHashSet<String>();
        for (int i = 0; i < characters.length; i++) {
            AsciiArtCharacter character = characters[i];
            Set<Character> possibleSubstitutions = character.possibleSubstitutions();
            for (Character substitution : possibleSubstitutions) {
                String sub = StringUtils.replaceChar(string, i, substitution);
                if (!sub.contains("?")) {
                    retVal.add(sub);
                }
            }
        }
        return retVal;
    }

    private static boolean isValidAsciiArt(String[] strings) {
        if (strings == null) {
            return false;
        }

        if (strings.length != 3) {
            return false;
        }

        int firstStringLen = strings[0].length();
        for (String string : strings)
        {
            if (string.length() != firstStringLen) return false;
            if (string.length() % 3 != 0) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if (hasUnrecognizedCharacter()) {
            return this.string + " ILL";
        }
        return this.string;
    }

    public String getString() {
        return string;
    }

    public boolean hasUnrecognizedCharacter() {
        return hasUnrecognizedCharacter;
    }
}
