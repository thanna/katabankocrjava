package com.travishanna.kata;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Travis Hanna
 */
public class CharacterUtils {
	
	private CharacterUtils() {
		/* static utility class, no need to instantiate me */
	}
	
    public static CharSequence toggleBetweenCharacterAndSpace (CharSequence sequence, int position, char toggleWith) {
        StringBuilder builder = new StringBuilder(sequence);
        if (builder.charAt(position) == ' ') {
            builder.replace(position, position + 1, Character.toString(toggleWith));
        }
        else {
            builder.replace(position, position + 1, " ");
        }
        return builder.toString();
    }
    
    public static CharSequence characterArrayToCharacterSequence (char[][] arr) {
        StringBuilder builder = new StringBuilder();
        for (char[] c : arr) {
        	builder.append(c);
        }
        return builder.toString();
    }

    public static char[][] stringArrayToCharArray (String[] strings) {
        char[][] retVal = new char[strings.length][];
        for (int i = 0; i < strings.length; i++) {
            retVal[i] = strings[i].toCharArray();
        }
        return retVal;
    }

    public static List<char[][]> splitMultiDimensionalArrayVertically (char[][] arr, int width) {
        List<char[][]> retVal = new ArrayList<char[][]>();

        int firstLength = arr[0].length;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].length != firstLength) {
                throw new IllegalArgumentException("Array must be rectangular");
            }
        }
        if (firstLength % width != 0) {
            throw new IllegalArgumentException("First dimension of array must be evenly divisible by width");
        }

        for (int i = 0; i < arr[0].length / width; i++) {
            char[][] chunk = new char[arr.length][width];
            for (int j = 0; j < arr.length; j++) {
                System.arraycopy(arr[j], i * width, chunk[j], 0, width);
            }
            retVal.add(chunk);
        }

        return retVal;
    }
}
