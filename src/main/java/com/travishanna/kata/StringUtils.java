package com.travishanna.kata;

/**
 * Author: Travis Hanna
 */
public class StringUtils {

    private StringUtils() {
        /* static utility class, don't instantiate me */
    }

    public static String replaceChar(String s, int pos, char newValue) {
        StringBuilder builder = new StringBuilder(s);
        builder.replace(pos, pos + 1, Character.toString(newValue));
        return builder.toString();
    }
}
