package com.travishanna.kata;

/**
 * Author: Travis Hanna
 * Represents an account number which can be verified via a checksum
 */
public class AccountNumber {
    private String number;

    public AccountNumber(String number) {
        this.number = number;
    }

    public boolean isValid() {
        if (calculateChecksum() == 0) {
            return true;
        }
        return false;
    }

    private short calculateChecksum() {
        short[] digits = reverseArray(convertToDigits());
        int total = 0;
        for (int i = 0; i < digits.length; i++) {
            total += (i+1) * digits[i];
        }
        return (short) (total % 11);
    }

    private short[] reverseArray(short[] arr) {
        short[] retVal = new short[arr.length];
        for (int i = arr.length - 1, j = 0; i >=0; i--, j++) {
            retVal[j] = arr[i];
        }
        return retVal;
    }

    private short[] convertToDigits() {
        short[] retVal = new short[this.number.length()];
        char[] chars = this.number.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            retVal[i] = Short.parseShort(Character.toString(chars[i]));
        }
        return retVal;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return this.number + " ERR";
        }
        return this.number;
    }

}
