package com.travishanna.kata;

import java.io.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This java class parses OCR files and outputs account numbers.  It can be executed from the command line and
 * will follow the UNIX convention of returning a 0 status for success and a non-zero status for failure.
 *
 * Author: Travis Hanna
 */
public class OCRProcessor {
    private static final int STATUS_CODE_SUCCESS = 0;
    private static final int STATUS_CODE_INVALID_ARGS = 1;
    private static final int STATUS_CODE_FILE_NOT_FOUND = 2;
    private static final int STATUS_CODE_FILE_NOT_READABLE = 3;
    private static final int STATUS_CODE_FILE_CORRUPT = 4;

    private static final int RECORD_LENGTH = 4;

    public static void main(String[] args) {
        int status = run(args);
        System.exit(status);
    }

    public static int run(String[] args) {
        if (args.length != 1) {
            outputUsage();
            return STATUS_CODE_INVALID_ARGS;
        }

        try {
            parseFile(args[0]);
        } catch (FileNotFoundException e) {
            System.err.println(args[0] + " not found");
            return STATUS_CODE_FILE_NOT_FOUND;
        } catch (IOException e) {
            System.err.println("Cannot read " + args[0]);
            return STATUS_CODE_FILE_NOT_READABLE;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
            return STATUS_CODE_FILE_CORRUPT;
        }

        return STATUS_CODE_SUCCESS;
    }

    private static void outputUsage() {
        System.out.println("Usage:");
        System.out.println("OCRProcessor input-file");
    }

    private static void parseFile(final String filename) throws IOException {
        FileReader fr = null;
        BufferedReader br = null;

        int i = 0;
        try
        {
        	fr = new FileReader(filename);
        	br = new BufferedReader(fr);

            String[] record = new String[3];
            String line;

            while ((line = br.readLine()) != null) {
                if (i % RECORD_LENGTH == RECORD_LENGTH - 1) {
                    outputRecord(record);
                }
                else {
                    record[i % RECORD_LENGTH] = line;
                }
                i++;
            }
        }
        catch (FileNotFoundException e) {
        	throw e;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error parsing file somewhere around line #" + i, e);
        }
        finally {
        	try { br.close(); } catch (Exception ex) { ex.printStackTrace(); }
        	try { fr.close(); } catch (Exception ex) { ex.printStackTrace(); }
        }
    }

    private static void outputRecord(final String[] record) {
        AsciiArtString parsedString = AsciiArtString.parseStringArray(record);
        Set<String> substitutions = findSubstitutions(parsedString);
        if (substitutions.size() > 0) {
            outputRecordWithSubstitutions(substitutions);
        }
        else if (parsedString.hasUnrecognizedCharacter()) {
            System.out.println(parsedString);
        }
        else {
            AccountNumber accountNumber = new AccountNumber(parsedString.getString());
            System.out.println(accountNumber);
        }
    }

    private static void outputRecordWithSubstitutions(Set<String> substitutions) {
        StringBuilder builder = new StringBuilder();
        for (String substitution : substitutions) {
            if (builder.length() == 0) {
                builder.append(substitution);
                if (substitutions.size() > 1) {
                    builder.append(" AMB [");
                }
            }
            else {
                builder.append("'").append(substitution).append("', ");
            }
        }
        if (substitutions.size() > 1) {
            builder.replace(builder.length() - 2, builder.length(), "]");
        }
        System.out.println(builder.toString());
    }

    private static Set<String> findSubstitutions(AsciiArtString parsedString)
    {
        Set<String> retVal = new LinkedHashSet<String>();
        Set<String> substitutions = parsedString.possibleSubstitutions();
        Set<String> validSubstitutions = new LinkedHashSet<String>();

        for (String substitution : substitutions) {
            AccountNumber accountNumber = new AccountNumber(substitution) ;
            if (accountNumber.isValid()) {
                validSubstitutions.add(accountNumber.toString());
            }
        }

        if (!parsedString.hasUnrecognizedCharacter()) {
            AccountNumber accountNumber = new AccountNumber(parsedString.toString());
            if (accountNumber.isValid() || validSubstitutions.size() > 1) {
                retVal.add(parsedString.toString());
            }
        }

        retVal.addAll(validSubstitutions);

        return retVal;
    }
}
