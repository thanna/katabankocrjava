KataBankOCR
===========
This repository holds a solution for [KataBankOCR](http://codingdojo.org/cgi-bin/wiki.pl?KataBankOCR) written by Travis Hanna.  I'm releasing this work as a git repository (as opposed to releasing only the final revision in a zip file) in order to show not just a solution to the problem, but the road I took for reaching a solution.  

The downside to this approach (for me), is that I'm exposing most of the mistakes that I made along the way in additon to any mistakes which remain embedded in the code.  I beg forgiveness for those extra mistakes.  I hope that they will be outweighed by:

* Assurance that I actually wrote the code and did not plagerize this solution from some random website. 	
* Evidence that I was writing tests first and code second (I intentionally commited some broken tests with unimplemented code to illustrate this)
* Many fearless refactorings made possible through adequate test coverage


Building
--------
I'm currently building this application with Maven 3.0.3 and Java 1.6.0_43 on MacOS 10.8.2. All other dependencies are being handled by Maven.  Running "mvn install" should build a fully-functioning application.

Usage
-----
The main artifact of this build is an executable jar.  If you are on a UNIX, Linux, or MacOS machine, you should be able to execute this jar via a shell script that I've dropped into the project root (OCRProcessor).  For example:

	./OCRProcessor src/test/resources/test-input.txt 

If you are on a windows box, you may want to look at the contents of the OCRProcessor file and pull the command out.

Notes
-----
I enjoyed this exercise.  It's fun to solve this sort of synthetic problem and introspect on your development process.  

I chose to test most of the application with straight junit tests.  It seemed best suited to the task at hand.  However, I also tossed in a couple of cucumber tests as pseudo user-acceptance tests.  

The user stories in the Kata were not written in what I would recognize as a user story format, making acceptance tests a bit more difficult to concieve.  They seemed more like a software spec that just happened to be written in a conversational tone.  In fact, I even noticed that in their test data examples, at the bottom, they refer to them as "Use Cases" (though they're also not written in what I would recognize as a typical use case format).  

Early on, I did pull in some unnecessary dependencies (guava and commons-cli) and implement some unnecessary methods (hashCode and equals) only to delete them later.  Oops.  I suppose that's the point in this sort of exercise.

Contact Info
------------
Travis Hanna  
travis.hanna@gmail.com  
614.304.1210  
[Résumé](https://bitbucket.org/thanna/katabankocrjava/downloads/TravisHanna.pdf)